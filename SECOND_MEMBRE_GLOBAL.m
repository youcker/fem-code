function [FG]=SECOND_MEMBRE_GLOBALE(MAIL,COOR,NMAI,NDL)
FG=zeros(NDL,1);
for IMAIL=1:NMAI
    X1=COOR(MAIL(IMAIL,1),1)
    X2=COOR(MAIL(IMAIL,2),1);
    H=X2-X1;
    [FE]=SECOND_MEMBRE_ELEM(H);
    for i=1:2
        ig=MAIL(IMAIL,1)-1+i;
        FG(ig)=FG(ig)+FE(i);
     end
end
end