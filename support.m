function [INDEX]=support(COOR,xi,h)
M=1;
   for I=1:size(COOR,1)
       rij=COOR(I,1)-xi;
       R=abs(rij)/h;
       if(R<=1)
           INDEX(M)=I;
           M=M+1;
       end
   end
       