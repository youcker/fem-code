function [KG]=MATRICE_GLOBALE(MAIL,COOR,NMAI,NDL)
KG=zeros(NDL,NDL);
for IMAIL=1:NMAI
    X1=COOR(MAIL(IMAIL,1),1);
    X2=COOR(MAIL(IMAIL,2),1);
    H=X2-X1;
    [KE]=MATRICE_ELEM(H);
    for i=1:2
        ig=MAIL(IMAIL,1)-1+i;
        for j=1:2
            jg=MAIL(IMAIL,1)-1+j;
            KG(ig,jg)=KG(ig,jg)+KE(i,j);
        end
    end
end