clear all
clc
A=1;
A1=0;
A2=1;
NNOEUD=1000;
[COOR,DX]=accoor(NNOEUD,A1,A2);
COOR
[MAIL,NMAI]=MAILLAGE(NNOEUD);
MAIL
NDL=NNOEUD;
KG=MATRICE_GLOBALE(MAIL,COOR,NMAI,NDL);
KG
FG=SECOND_MEMBRE_GLOBALE(MAIL,COOR,NMAI,NDL)
[KG,FG]=ACLIAI(KG,FG,NDL)
u=KG\FG
for i=1:NNOEUD
    xi=COOR(i,1);
    uexact(i)=(COOR(i,1)-1)*COOR(i,1)*A/2;
end
uexact
plot(COOR(:,1),u,'g-*');
hold on
plot(COOR(:,1),uexact,'r-o');




